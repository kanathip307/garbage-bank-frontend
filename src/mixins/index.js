import moment from 'moment'
import swal from 'sweetalert2'

export const mixins = {
  methods: {
    formatDate (time) {
      const formatDate = moment(time).format('YYYY-MM-DD HH:mm')
      return formatDate
    },
    convertDateToISO (dateString) {
      const date = moment(dateString, 'YYYY-MM-DD HH:mm').utc().format()
      return date
    },
    formatPrice (value) {
      const priceDecimal = Number(value).toFixed(2)
      const formatPrice = priceDecimal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
      return formatPrice
    },
    formatNumber (value) {
      const numberDecimal = Number(value)
      const formatNumber = numberDecimal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
      return formatNumber
    },
    async sweatAlert ({
      position = 'top-end',
      showConfirmButton = false,
      timer = 2000,
      timerProgressBar = true,
      icon = 'success',
      title = 'No Title'
    }) {
      const Toast = swal.mixin({
        toast: true, position, showConfirmButton, timer, timerProgressBar,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })

      await Toast.fire({
        icon: icon,
        title: title
      })

      return await new Promise((resolve) => {
        return resolve()
      })
    }
  }
}