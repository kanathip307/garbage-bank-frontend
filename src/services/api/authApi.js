import instance from '@/services'

export default {
  login: (data) => {
    return instance.post('auth/login', data).then(response => response.data)
  },
  register: (data) => {
    return instance.post('auth/register', data).then(response => response.data)
  },
  logout: () => {
    return instance.post('auth/logout').then(response => response.data)
  },
  checkToken: (token) => {
    return instance.post('auth/checkToken', token).then(response => response.data)
  }
}
