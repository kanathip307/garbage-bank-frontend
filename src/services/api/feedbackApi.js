import instance from '@/services'

export default {
  find: () => {
    return instance.get('feedback').then(response => response.data)
  },
  findById: (id) => {
    return instance.get(`feedback/${id}`).then(response => response.data)
  },
  create: (data) => {
    return instance.post('feedback', data).then(response => response.data)
  },
  update: (id, data) => {
    return instance.put(`feedback/${id}`, data).then(response => response.data)
  },
  delete: (id) => {
    return instance.delete(`feedback/${id}`).then(response => response.data)
  }
}
