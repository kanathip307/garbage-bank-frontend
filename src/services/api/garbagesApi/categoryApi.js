import instance from '@/services'

export default {
  find: () => {
    return instance.get('garbages_category').then(response => response.data)
  },
  findById: (id) => {
    return instance.get(`garbages_category/${id}`).then(response => response.data)
  },
  create: (data) => {
    return instance.post('garbages_category', data).then(response => response.data)
  },
  update: (id, data) => {
    return instance.put(`garbages_category/${id}`, data).then(response => response.data)
  },
  delete: (id) => {
    return instance.delete(`garbages_category/${id}`).then(response => response.data)
  }
}
