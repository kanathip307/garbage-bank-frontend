import instance from '@/services'

export default {
  find: async () => {
    return await instance.get('garbages').then(response => response.data)
  },
  findById: async (id) => {
    return await instance.get(`garbages/${id}`).then(response => response.data)
  },
  create: async (data) => {
    return await instance.post('garbages', data).then(response => response.data)
  },
  update: async (id, data) => {
    return await instance.put(`garbages/${id}`, data).then(response => response.data)
  },
  delete: async (id) => {
    return await instance.delete(`garbages/${id}`).then(response => response.data)
  }
}
