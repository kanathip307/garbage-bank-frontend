import instance from '@/services'

export default {
  find: () => {
    return instance.get('news').then(response => response.data)
  },
  findById: (id) => {
    return instance.get(`news/${id}`).then(response => response.data)
  },
  create: (data) => {
    return instance.post('news', data).then(response => response.data)
  },
  update: (id, data) => {
    return instance.put(`news/${id}`, data).then(response => response.data)
  },
  delete: (id) => {
    return instance.delete(`news/${id}`).then(response => response.data)
  }
}
