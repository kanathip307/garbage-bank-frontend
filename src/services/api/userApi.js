import instance from '@/services'

export default {
  find: () => {
    return instance.get('users').then(response => response.data)
  },
  findById: (id) => {
    return instance.get(`users/${id}`).then(response => response.data)
  },
  update: (id, data) => {
    return instance.put(`users/${id}`, data).then(response => response.data)
  },
  delete: (id) => {
    return instance.delete(`users/${id}`).then(response => response.data)
  }
}
