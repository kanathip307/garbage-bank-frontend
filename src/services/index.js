import axios from 'axios'

const instance = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  withCredentials: false,
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'ngrok-skip-browser-warning': true
  }
})

instance.interceptors.request.use((config) => {
  try {
    const storage = localStorage.getItem('user')
    const storageJSON = storage ? JSON.parse(storage) : false

    if (storageJSON && storageJSON.token) {
      config.headers.Authorization = `Bearer ${storageJSON.token}`
    }
  } catch (error) {
    console.log('error from API instance', error)
  }

  return config
})

export default instance
