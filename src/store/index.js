import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    feedback: {
      title: '',
      detail: '',
      rules: {
        title: [v => !!v || 'ต้องระบุ'],
        detail: [v => !!v || 'ต้องระบุ']
      }
    }
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  }
})
