import authApi from '@/services/api/authApi'

async function authGuard (to, from, next) {
  try {
    const storedUserData = localStorage.getItem('user')
    const parsedUserData = storedUserData ? JSON.parse(storedUserData) : null
    const userToken = parsedUserData ? parsedUserData.token : false
    const userRole = parsedUserData ? parsedUserData.user.role : false
    const isTokenValid = parsedUserData ? await authApi.checkToken(parsedUserData) : false

    const BORestrictedRoutes = ['DashboardPage', 'ManageGarbageDepositsPage', 'ManageNewsPage', 'ManageGarbagePage', 'ManageCategoryPage', 'ManageFeedbackPage']
    const FORestrictedRoutes = ['HomePage', 'GarbagePage', 'ContactPage', 'FeedbackPreAuthPage']

    if (isTokenValid.data && userToken) {
      if (userRole === 1 && BORestrictedRoutes.includes(to.name) && from.name !== 'HomePage') {
        next({ name: 'HomePage' })
        return
      }
      if (userRole === 1 && FORestrictedRoutes.some(route => to.name === route) && to.name !== 'TransactionHistoryPage') {
        next({ name: 'TransactionHistoryPage' })
        return
      }
      if (userRole === 0 && to.name === 'AuthBackOfficePage' && from.name !== 'DashboardPage') {
        next({ name: 'DashboardPage' })
        return
      }
    } else {
      if (to.name !== 'HomePage') {
        next({ name: 'HomePage' })
        return
      }
    }
    next()
  } catch (error) {
    console.error('Error in Auth Guard:', error)
  }
}

export default authGuard